(function ($, Drupal) {
  function megadropdown($menuitems) {
    $menuitems.each(function (i, item) {
      item = $(item);
      if (item.closest('.pd-megadropdown').length > 0) {
        return;
      }
      var dropdown = $(".pd-dropdown-block[data-menu-entry='" + item.attr('data-drupal-link-system-path') + "']");
      if (dropdown.length > 0) {
        item.closest('li').find('ul').addClass('pd-megadropdown-hide');
        item.closest('li').on("mouseenter", function (element) {
          $(".pd-dropdown-block").removeClass('pd-show');
          dropdown.addClass("pd-show");
        });
        $(dropdown).on("mouseleave", function () {
          dropdown.removeClass("pd-show");
        });
      } else {
        item.closest('li').on("mouseenter", function (element) {
          $(".pd-dropdown-block").removeClass('pd-show');
        });
      }
    });

    $('.megadrop-close').on('click', function () {
      $(".pd-dropdown-block").removeClass('pd-show');
    });
  };

  Drupal.behaviors.pagedesigner_megadropdown = {
    attach: function (context, settings) {
      megadropdown( $('.pd-megadropdown-menu [data-drupal-link-system-path]'));
    }
  };
})(jQuery, Drupal);
