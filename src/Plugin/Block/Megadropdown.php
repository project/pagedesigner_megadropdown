<?php

namespace Drupal\pagedesigner_megadropdown\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\pagedesigner_parts\Plugin\Block\PagedesignerPart;

/**
 * Provides a Pagedesigner megadropdown block.
 *
 * @Block(
 *   id = "pagedesigner_megadropdown",
 *   admin_label = @Translation("Pagedesigner megadropdown"),
 * )
 */
class Megadropdown extends PagedesignerPart {

  /**
   * Extend the pagedesigner part render array.
   *
   * Add the megadropdown suffix/prefix html, including the matching link.
   *
   * @return array
   *   The extended render array.
   */
  public function build() {
    $dropdownNode = NULL;
    $build = [];
    $build[] = parent::build();
    if (isset($this->configuration['dropdownNode'])) {
      $dropdownNode = $this->configuration['dropdownNode'];
    }
    $build['#prefix'] = '<div class="pd-dropdown-block" data-menu-entry="node/' . $dropdownNode . '">';
    $build['#suffix'] = '</div>';
    return $build;
  }

  /**
   * Extend the pagedesigner part form.
   *
   * Add an option to set the dropdown node.
   *
   * @return array
   *   The extended form array.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $node = NULL;
    $dropdownNode = -1;
    if (isset($this->configuration['dropdownNode'])) {
      $dropdownNode = $this->configuration['dropdownNode'];
      $node = Node::load($dropdownNode);
    }
    $form['dropdownNode'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Dropdown node'),
      '#target_type' => 'node',
      '#selection_settings' => ['target_bundles' => NULL],
      '#tags' => TRUE,
      '#size' => 30,
      '#maxlength' => 1024,
      '#default_value' => $node,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * Save the dropdown node to the configuration.
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['dropdownNode'] = $form_state->getValue('dropdownNode')[0]["target_id"];
  }

}
